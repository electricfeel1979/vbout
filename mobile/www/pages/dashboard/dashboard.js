/**
 * Created by kevin on 04/03/2016.
 */

'use strict';

angular.module('vbout.dashboard', [])

  .config(function($stateProvider) {
    $stateProvider.state('app.dashboard', {
      url: '/dashboard',
      views: {
        'menuContent': {
          templateUrl: 'pages/dashboard/dashboard.html',
          controller: 'DashboardCtrl'
        }
      }
    });
  })

  .controller('DashboardCtrl', [function($scope) {
    console.log('DashboardCtrl');
  }]);
